export interface MovieSummary {
    Title: string,
    Year: string,
    imdbID: string,
    Type: "movie" | "series",
    Poster: string,
}

export interface Query {
    searchText: string,
    page: number,
}