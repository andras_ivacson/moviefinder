import { Subject, Observable, of } from "rxjs";
import { switchMap, map, debounceTime } from "rxjs/operators";
import { ajax } from "rxjs/ajax";
import { MovieSummary, Query } from "../model/model.interface";

interface MovieQueryResult {
    Search: MovieSummary[],
    totalResults: string,
    Response: 'true'
}
// TODO: move this to ENV
const HOST = 'https://omdbapi.com';
const INPUT_DEBOUNCE_TIME = 300;

export class MovieFinderService {
    public searchText$ = new Subject<string>();

    getData(searchText$: Subject<Query>): Observable<MovieSummary[]> {
        const stream = searchText$
            .pipe(
                debounceTime(INPUT_DEBOUNCE_TIME),
                switchMap(query => {
                    if (query.searchText.length < 3) {
                        return of({Search: [] as MovieSummary[]} as MovieQueryResult)
                    }
                    return ajax.getJSON<MovieQueryResult>(`${HOST}/?s=${query.searchText}&page=${query.page}&apiKey=c6212423`);
                }),
                map((result: MovieQueryResult) => result.Search)
            );
        return stream;
    }
}