import React from 'react';
import { HeaderComponentProps } from '../header/header';
import { Search } from '@material-ui/icons';
import './search-input.scss';
import { Box } from '@material-ui/core';

export const SearchInput = (props: HeaderComponentProps) => {
    function update(event: React.ChangeEvent<HTMLInputElement>) {
        props.update(event.currentTarget.value);
    }

    return (
        <div>
            <Box className="search-input-box" bgcolor="action.disabledBackground">
                <div className="input-wrapper">
                    <input className="search-input" type="text" placeholder="start typing..." onChange={update}></input>
                </div>
                <Search className="search-icon"/>
            </Box>
    
        </div>
    );
};