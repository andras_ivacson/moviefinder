import React, { FC } from 'react'
import { Grid, Box } from '@material-ui/core'

export interface HeaderComponentProps {
    update: (value: string) => void
    children?: any
    className?: string
}

export const Header = (WrappedComponent: FC<HeaderComponentProps>) => ({
    className,
    ...props
}: HeaderComponentProps) => {
    return (
        <Box className={className}>
            <Grid
                container
                direction="row"
                alignItems="center"
                justify="center"
                className="h100"
            >
                <WrappedComponent {...props}></WrappedComponent>
            </Grid>
        </Box>
    )
}
