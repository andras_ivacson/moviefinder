import React, { FC, useState } from 'react'
import { MovieSummary } from '../../model/model.interface'
import { Grid, Typography, makeStyles, Box } from '@material-ui/core'
import { MovieCard } from '../movie-card/movie-card'
export interface MovieListProps {
    movies: MovieSummary[]
    onloadMore: (event: any) => void
}

const useStyles = makeStyles({
    listItem: {
        width: '100%',
        marginBottom: '8px',
        cursor: 'pointer',
    },
    selectedListItem: {
        width: '100%',
        marginBottom: '8px',
    },
})

export const MovieList: FC<MovieListProps> = props => {
    const classes = useStyles()
    const [selection, setSelection] = useState(null as MovieSummary | null)

    const List = React.memo(({ items }: { items: any[] }) => (
        <Box className="layout--column layout--column__scrollable">
            {items &&
                items.map((item: MovieSummary) => (
                    <Typography
                        key={item.imdbID}
                        noWrap={true}
                        color={
                            selection === item ? 'textSecondary' : 'textPrimary'
                        }
                        className={
                            selection === item
                                ? classes.selectedListItem
                                : classes.listItem
                        }
                        onClick={() =>
                            setSelection(item === selection ? null : item)
                        }
                    >
                        {item.Title}
                    </Typography>
                ))}
            {items && items.length ? (
                <p className={classes.listItem} onClick={props.onloadMore}>
                    <i>load more...</i>
                </p>
            ) : (
                <></>
            )}
        </Box>
    ))

    

    const CardSelector = React.memo(
        ({
            items,
            selected,
        }: {
            items: any[]
            selected: MovieSummary | null
        }) => (
            <Grid
                container
                direction="row"
                spacing={4}
                className="h100"
                justify="center"
            >
                <List items={items}></List>
                <MovieCard data={selection} className="layout--column"></MovieCard>
            </Grid>
        )
    )
    return (
        <CardSelector items={props.movies} selected={selection}></CardSelector>
        
    )
}
