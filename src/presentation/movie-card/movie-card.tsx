import React, { FC } from 'react'
import { MovieSummary } from '../../model/model.interface'
import { Box, makeStyles, Card, Typography } from '@material-ui/core'

import './movie-card.scss'

export interface MovieCardProps {
    data: MovieSummary | null
    className: string
}

const useStyles = makeStyles({
    card: {
        height: '100%',
        display: 'inline-block',
        position: 'relative',
    },
    cover: {
        height: '100%',
        maxWidth: '100%',
        backgroundSize: 'cover',
    },
    summary: {
        position: 'absolute',
        opacity: 0.9,
        bottom: 0,
        left: 0,
        right: 0,
        height: '30%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
    },
    summaryTitle: {
        padding: '0 16px',
    },
})

export const MovieCard: FC<MovieCardProps> = (props: MovieCardProps) => {
    const classes = useStyles()
    return props.data ? (
        <Box className={props.className}>
            <Card className={classes.card} raised={true}>
                <img
                    className={classes.cover}
                    src={props.data.Poster}
                    alt="coverArt"
                />
                <Box className={classes.summary}>
                    <Typography
                        className={classes.summaryTitle}
                        variant="h5"
                        component="h5"
                        color="textSecondary"
                    >
                        {props.data?.Title}
                    </Typography>
                </Box>
            </Card>
        </Box>
    ) : (
        <></>
    )
}
