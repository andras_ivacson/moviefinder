import React, { useState, FC, useCallback, useMemo } from 'react'
import { Header } from '../../presentation/header/header'
import { SearchInput } from '../../presentation/search-input/search-input'
import { MovieList } from '../../presentation/movie-list/movie-list'
import { MovieFinderService } from '../../service/movie-finder.service'
import { MovieSummary, Query } from '../../model/model.interface'
import { Subject } from 'rxjs'
import {
    Container,
    CircularProgress,
    Grid,
} from '@material-ui/core'
import './movie-finder.scss'

export interface MovieFinderProps {}

const HeaderWithSearchInput = Header(SearchInput)


export const MovieFinder: FC<MovieFinderProps> = () => {
    const [searchResult, setSearchResult] = useState([] as MovieSummary[])
    const [loading, setLoading] = useState(false)
    const [page, setPage] = useState(1)
    const [searchText, setSearchText] = useState('')

    const searchText$ = useMemo(() => {
        const stream = new Subject<Query>()
        new MovieFinderService().getData(stream).subscribe(_searchResult => {
            setSearchResult((searchResult) => [...searchResult, ..._searchResult])
            setLoading(false)
        })
        return stream
    }, [])

    const onSearchUpdated = useCallback(
        searchText => {
            searchText$.next({ searchText, page })
            if (searchText.length > 2) {
                setLoading(true)
                setPage(1)
                setSearchResult([])
                setSearchText(searchText)
            }
        },
        [searchText$, page]
    )
    const Content = useMemo(() => {
        return loading ? (
            <CircularProgress />
        ) : (
            <MovieList
                movies={searchResult}
                onloadMore={() => {
                    setPage(page => page + 1)
                    searchText$.next({ searchText, page })
                }}
            ></MovieList>
        )
    }, [loading, searchResult, searchText, searchText$, page])

    return (
        <Container maxWidth="md" className="h100">
            <Grid className="h100" container direction="column" wrap="nowrap">
                <HeaderWithSearchInput  className="app--header"
                    update={onSearchUpdated}
                ></HeaderWithSearchInput>
                <Grid
                    className="app--main"
                    container
                    direction="row"
                    justify="center"
                    alignItems="flex-start"
                >
                    {Content}
                </Grid>
            </Grid>
        </Container>
    )
}
