import React from 'react'
import { MovieFinder } from './container/movie-finder/movie-finder'
import './App.scss'
import { ThemeProvider, createMuiTheme, CssBaseline } from '@material-ui/core'

const theme = createMuiTheme({
    palette: {
        type: 'dark',
    },
})

function App() {
    return (
        <ThemeProvider theme={theme}>
            <CssBaseline />
            <div className="App h100">
                <MovieFinder></MovieFinder>
            </div>
        </ThemeProvider>
    )
}

export default App
